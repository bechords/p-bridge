// ---------------------------------------
// 変数定義
// ---------------------------------------
/* 画面遷移フラグ(0:自、1:他) */
var other_screen = 1;
/* 処理中フラグ */
var loading = '0';
/* テーブルId(PC or PC以外) */
var table_id;
/* 上記テーブルの選択行 */
var select_row;
// チェック結果
var result = '1';

// ---------------------------------------
// 追加
// ---------------------------------------
/* PC／PC以外切替 */
$( 'input[name="optionsRadios"]:radio' ).change( function() {
    // 追加時 入力制御
    if ($( this ).val() == "option2") {
        $("input.pc").attr("readonly", true);
        $("input.pc").css("cssText", "background-color: #A9A9A9 !important;");
        $("#itemMaker").html("メーカー");
        $("#itemModel").html("品名");
        $("#no").html($("#nonpc_table tbody").children().length + 1);
    } else {
        $("input.pc").attr("readonly", false);
        $("input.pc").css("cssText", "background-color:transparent !important;");
        $("#itemMaker").html("メーカー");
        $("#itemModel").html("型番／モデル名");
        $("#no").html($("#pc_table tbody").children().length + 1);
    }
});

/* PCリストへの追加処理 */
function add_pc(machineNo,editor){
    var no = $("#pc_table tbody").children().length + 1;
    $("#pc_table").append(
            $("<tr></tr>")
                .append($('<td style="display:none;" id="machineNo"></td>').text(machineNo)) // key
                .append($('<td class="aligncenter">').text(no)) // no
                .append($('<td class="aligncenter editable" id="assetNo"></td>').text(editor[0].cells[2].firstChild.value)) // 資産コード
                .append($('<td class="aligncenter editable" id="machineVendor"></td>').text(editor[0].cells[3].firstChild.value)) // メーカ
                .append($('<td class="aligncenter editable" id="typeCd"></td>').text(editor[0].cells[4].firstChild.value)) // 型番モデル
                .append($('<td class="aligncenter editable" id="machineSereal"></td>').text(editor[0].cells[5].firstChild.value)) // シリアル
                .append($('<td class="aligncenter editable sup-cpu" id="cpu"></td>').text(editor[0].cells[6].firstChild.value)) // CPU
                .append($('<td class="aligncenter editable" id="ram"></td>').text(editor[0].cells[7].firstChild.value)) // メモリ
                .append($('<td class="aligncenter editable" id="capacity1"></td>').text(editor[0].cells[8].firstChild.value)) // HDD
                .append($('<td class="aligncenter editable" id="note"></td>').text(editor[0].cells[10].firstChild.value)) // 備考1
                .append($('<td class="aligncenter editable" id="note2"></td>').text(editor[0].cells[11].firstChild.value)) // 備考2
                .append($('<td class="aligncenter"><label class="aligncenter"><input type="checkbox" name="pcwhite" value="' + machineNo + '"></label></td>')) // PC WHITE
                .append($('<td class="aligncenter"><label class="aligncenter"><input type="checkbox" name="delete" value="' + machineNo + '"></label></td>'))
    );
}

/* PC以外リストへの追加処理 */
function add_nonpc(machineNo,editor){
    var no = $("#nonpc_table tbody").children().length + 1;
    $("#nonpc_table").append(
            $("<tr></tr>")
                .append($('<td style="display:none;" id="machineNo"></td>').text(machineNo)) // key
                .append($('<td class="aligncenter"></td>').text(no)) // no
                .append($('<td class="aligncenter editable" id="typeCd"></td>').text(editor[0].cells[4].firstChild.value)) // 型番モデル
                .append($('<td class="aligncenter editable" id="quantity"></td>').text("1")) // 数量
                .append($('<td class="aligncenter editable" id="note"></td>').text(editor[0].cells[10].firstChild.value)) // 備考1
                .append($('<td class="aligncenter editable" id="note2"></td>').text(editor[0].cells[11].firstChild.value)) // 備考2
                .append($('<td class="aligncenter"><label class="aligncenter"><input type="checkbox" name="delete" value="' + machineNo + '"></label></td>'))
    );
}

/* 項目チェック */
function allItemsNullorEmpty(editor, button){
    var no2 = editor[0].cells[2].firstChild.value; // 資産コード
    var no3 = editor[0].cells[3].firstChild.value; // メーカ
    var no4 = editor[0].cells[4].firstChild.value; // 型番モデル
    var no5 = editor[0].cells[5].firstChild.value; // シリアル
    var no6 = editor[0].cells[6].firstChild.value; // CPU
    var no7 = editor[0].cells[7].firstChild.value; // メモリ
    var no8 = editor[0].cells[8].firstChild.value; // HDD
    var no9 = editor[0].cells[9].firstChild.value; // 数量
    var no10 = editor[0].cells[10].firstChild.value; // 備考1
    var no11 = editor[0].cells[11].firstChild.value; // 備考2

    if(button){
        if(no2==''&&no3==''&&no4==''&&no5==''&&no6==''&&no7==''&&no8==''&&no10==''&&no11==''){
            //エラーが発生した場合、エラーメッセージを表示する要素を組立てる。
            var elem = '<div class="alert alert-error"><ul>';
            elem += '<li>数量以外のいづれかの項目を入力してください。</li>';
            elem += '</ul></div>';
            $('#ajaxErrorMessage_add').html(elem);
            return false;
        }
    }else{
        if(no4==''&&no9==''&&no10==''&&no11==''){
            //エラーが発生した場合、エラーメッセージを表示する要素を組立てる。
            var elem = '<div class="alert alert-error"><ul>';
            elem += '<li>数量以外のいづれかの項目を入力してください。</li>';
            elem += '</ul></div>';
            $('#ajaxErrorMessage_add').html(elem);
            return false;
        }
    }
    return true;
}

/* [追加]ボタン押下時の処理 */
$('#add_form').submit(function(event) {

    // submitキャンセル
    event.preventDefault();

    // ローディング
    unavail();

    // エディタ
    var editor = document.getElementById("add_table").tBodies[0].rows;

    // チェック
    if(!allItemsNullorEmpty(editor, $("#optionsRadios1").is(':checked'))){
        avail();
        return false;
    }

    // 機器管理番号
    var machineNo = editor[0].cells[0].innerText;
    $('#add_machineNo').val(machineNo);

    // 数量(未入力の場合、「1」とする)
    var quantity;
    if(editor[0].cells[9].firstChild.value == ""){
        editor[0].cells[9].firstChild.value = 1;
    }
    quantity = editor[0].cells[9].firstChild.value;

    // ラジオボタン
    $('#add_machineNo').val(machineNo);
    if($("#optionsRadios1").is(':checked')){
        $('#add_table_id').val('pc_table');
    }else if($("#optionsRadios2").is(':checked')){
        $('#add_table_id').val('nonpc_table');
    }

    $.ajax({
        url : 'check',
        type : 'post',
        data :$('#add_form').serializeArray(),
        success: function(json, status, xhr) {

            var redctUrl = xhr.getResponseHeader("AjaxRedirect");
            if (redctUrl != null){
                // リダイレクトURLが設定されていれば追跡
                doMyself();
                window.location.href = redctUrl;
            }else{
                if( json == ""){
                    $('#ajaxErrorMessage_add').empty();

                    // DBへ追加
                    addTemp(machineNo, quantity, editor);

                }else{
                    var jsonData = $.parseJSON(json);

                    if(jsonData.status==false){
                         //エラーが発生した場合、エラーメッセージを表示する要素を組立てる。
                         var elem = '<div class="alert alert-error"><ul>';
                         for(i in jsonData.messages){
                          elem += '<li>'+jsonData.messages[i]+'</li>';
                         }
                         elem += '</ul></div>';
                         //Ajax処理のエラーメッセージ表示域に、エラーメッセージを挿入する。
                         $('#ajaxErrorMessage_add').html(elem);
                    }
                }
            }

            // 解放
            avail();
        },
        error: function(){
            alert("通信に失敗しました。\nシステム管理者に連絡してください。");
            return;
        }
    });
});

/*
 * Ajax：[追加]押下
 */
function addTemp(machineNo, quantity, editor){
    $.ajax({
        url : 'addTemp',
        type : 'post',
        data :$('#add_form').serializeArray(),
        success: function(data){

            var list = data.split(',');
            // 行作成
            for( var i=0; i < quantity ; i++ ){
                if($("#optionsRadios1").is(':checked')){
                    // 行追加
                    add_pc(list[i], editor);
                    $("#no").html($("#pc_table tbody").children().length + 1);
                }else if($("#optionsRadios2").is(':checked')){
                    // 行追加
                    add_nonpc(list[i], editor);
                    $("#no").html($("#nonpc_table tbody").children().length + 1);
                }

                // 機器管理番号を設定する
                editor[0].cells[0].innerText = list[i];
            }

            // チェックボックス反映
            $("input:checkbox").not('[data-no-uniform="true"],#uniform-is-ajax').uniform();

            // 更新を促すメッセージを表示する
            var updateMessage = '<div class="alert alert-success">編集内容を確定するには、更新ボタンを押してください。</div>';
            $('#UpdateMessage').html(updateMessage);

            $('#edit_flg').val('1');
        },
        error: function(xhr, textStatus, errorThrown){
            alert("通信に失敗しました。\nシステム管理者に連絡してください。");
        },
        complete : function(data) {
            avail();
        }
    });
}

// ---------------------------------------
// 更新
// ---------------------------------------
/* [更新]ボタン押下時の処理 */
function update(){
    var form = document.getElementById('a0100110');
    form.action = '/pbridge/asset/a0100110/update';
    form.submit();
}

// ---------------------------------------
// 編集
// ---------------------------------------
function editUpdate(request_no, machine_no, item_name, value, err_place){
    // ローディング
    unavail();

    // エラーメッセージ削除
    $(err_place).empty();

    // Ajax処理
    $.ajax({
      url: 'edit',
      type:'post',
      data: {request_no: request_no, machine_no: machine_no, item_name: item_name, value: value},
      success: function(message, status, xhr) {

          var redctUrl = xhr.getResponseHeader("AjaxRedirect");
          if (redctUrl != null){
              // リダイレクトURLが設定されていれば追跡
              doMyself();
              window.location.href = redctUrl;
          }else{
              if( message != "" ){
                  //エラーが発生した場合、エラーメッセージを表示する要素を組立てる。
                  var elem = '<div class="alert alert-error"><ul>';
                  elem += '<li>'+message+'</li>';
                  elem += '</ul></div>';
                  //Ajax処理のエラーメッセージ表示域に、エラーメッセージを挿入する。
                  $(err_place).html(elem);
                  return;
              }
          }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("通信に失敗しました。\nシステム管理者に連絡してください。");
      },
      complete : function(xhr, status) {

          // 更新を促すメッセージを表示する
          var updateMessage = '<div class="alert alert-success">編集内容を確定するには、更新ボタンを押してください。</div>';
          $('#UpdateMessage').html(updateMessage);

          // エディットフラグ更新
          $('#edit_flg').val('1');

          // 解放
          avail();
      }
    });
}

// ---------------------------------------
// 削除
// ---------------------------------------
/* [削除]ボタン押下時の処理 */
function remove(){
    var del_values = "";
    $("[name=delete]:checked").each(function(){
        del_values += $(this).val();
        del_values += ",";
    })
    document.getElementById("del_values").value = del_values.slice(0, -1);
    var form = document.getElementById('a0100110');
    form.action = '/pbridge/asset/a0100110/delete';
    form.submit();
}

//---------------------------------------
//PC White対象更新
//---------------------------------------
/* [PC White]ボタン押下時の処理 */
function pcwhite(){
 var pcwhite_values = "";
 $("[name=pcwhite]:checked").each(function(){
	 pcwhite_values += $(this).val();
	 pcwhite_values += ",";
 })
 document.getElementById("pcwhite_values").value = pcwhite_values.slice(0, -1);
 var form = document.getElementById('a0100110');
 form.action = '/pbridge/asset/a0100110/updatePcWhite';
 form.submit();
}

// ---------------------------------------
// その他
// ---------------------------------------
/* 自画面遷移の時にフラグ初期化 */
function doMyself(){
    other_screen = 0;
}

/* ページ遷移前のイベントハンドラ */
$(window).bind('beforeunload', function(event) {
    if($('#edit_flg').val() == 1){
        // 一時データ適用遷移じゃなければ表示
        if($('#temporary_submit').val() == 0 && other_screen == 1){
            return "\n確定していない編集中のデータが存在します。\n編集中データは保存されません。";
        }
    }
});

/* ボタン活性／非活性 */
function avail(){
    $('button').removeAttr('disabled');
    $('a').removeAttr('disabled');
    $("#loading").empty();
    loading = '0';
}

/* ペイン開閉 */
$("#btnClosePc").click(function(){
    $("#pcBox").slideToggle(500);
});
$("#btnCloseNonPc").click(function(){
    $("#nonPcBox").slideToggle(500);
});
$("#btnCloseAdd").click(function(){
    $("#addBox").slideToggle(500);
});

/* ソート */
$(function(){
    $('table').tablesorter({
        widgets        : ['zebra', 'columns'],
        usNumberFormat : false,
        sortReset      : true,
        sortRestart    : true
    });
});




