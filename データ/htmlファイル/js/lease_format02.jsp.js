$(document).on('click', '#delete', function(){
    $(this).closest('tr').remove();
});

$(document).on('click', '#add', function(){
$("#list-table").append(
    $("<tr></tr>")
        .append('<td class="alignright">*</td>')
        .append('<td><input type="text" class="non-border" value=""></td>')
        .append('<td><input type="text" class="non-border" value=""></td>')
        .append('<td class="alignright"><input type="text" class="non-border" value=""></td>')
        .append('<td class="alignright"><input type="text" class="non-border" value=""></td>')
        .append('<td class="alignright"><input type="text" class="non-border" value=""></td>')
        .append('<td><input type="text" class="non-border" value=""></td>')
        .append('<td class="aligncenter"><a href="#" class="btn btn-primary" id="delete">削除</a></td>')
    );
});

function setCustomerInfo(jsonData) {
    // 会社名
    if($("#checkCustomerName1").prop('checked')) {
        $('#contractCompany').val(jsonData.info.customerName1);
    }else{
        $('#contractCompany').val('');
    }
    // 部署名
    if($("#checkCustomerName2").prop('checked')) {
        $('#contractDepartment').val(jsonData.info.customerName2);
    }else{
        $('#contractDepartment').val('');
    }
    // 住所
    var address = '';
    if($("#checkAddress1").prop('checked')) {
        address += jsonData.info.address1;
    }
    if($("#checkAddress2").prop('checked')) {
        address += jsonData.info.address2;
    }
    if($("#checkAddress3").prop('checked')) {
        address += jsonData.info.address3;
    }
    $('#contractAddress').val(address);
    // TEL
    if($("#checkTel").prop('checked')) {
        $('#contractTel').val(jsonData.info.tel);
    }else{
        $('#contractTel').val('');
    }
    // FAX
    if($("#checkFax").prop('checked')) {
        $('#contractFax').val(jsonData.info.fax);
    }else{
        $('#contractFax').val('');
    }
    // 担当者
    if($("#checkStaff").prop('checked')) {
        if (jsonData.staffSize == 1) {
            $('#contractStaff').val(jsonData.staff[0].staff);
        }
    }else{
        $('#contractStaff').val('');
    }
    // 担当者名補完
    $( ".supply-staff" ).autocomplete({
    	source: function(request, response){
            var staffList = [];
            for(i in jsonData.staff){
                staffList.push(jsonData.staff[i].staff);
                response(staffList);
            }
    	},
    	autoFocus: false,
    	delay: 100,
    	minLength: 1,
    	focus : function(e, ui) {
    	    autocomp_edit_flg = 1;
    	},
    	select : function(e, ui) {
    	    autocomp_edit_flg = 0;
    	}
    });
}
