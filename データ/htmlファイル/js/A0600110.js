// ---------------------------------------
// 変数定義
// ---------------------------------------
/* 処理中フラグ */
var loading = '0';

// ---------------------------------------
// 編集
// ---------------------------------------
function editUpdate(request_no, machine_no, item_name, value, err_place, self){
    // ローディング
    unavail();

    // エラーメッセージ削除
    $(err_place).empty();

    // Ajax処理
    $.ajax({
      url: 'edit',
      type:'post',
      data: {request_no: request_no, machine_no: machine_no, item_name: item_name, value: value},
      success: function(message, status, xhr) {
          var inputVal = value;

          var redctUrl = xhr.getResponseHeader("AjaxRedirect");
          if (redctUrl != null){
              // リダイレクトURLが設定されていれば追跡
              doMyself();
              window.location.href = redctUrl;
          }else{
              if( message != "" ){
                  //エラーが発生した場合、エラーメッセージを表示する要素を組立てる。
                  var elem = '<div class="alert alert-error"><ul>';
                  elem += '<li>'+message+'</li>';
                  elem += '</ul></div>';
                  //Ajax処理のエラーメッセージ表示域に、エラーメッセージを挿入する。
                  $(err_place).html(elem);

                  // 元の値に戻す
                  inputVal = self.defaultValue;
              }
          }

          if(inputVal.length != 0){
              inputVal += ' 円';
          }
          //編集が終わったらtextで置き換える
          $(self).parent().removeClass('on').text(inputVal);

      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("通信に失敗しました。\nシステム管理者に連絡してください。");
      },
      complete : function(xhr, status) {
          // 解放
          avail();
      }
    });
}

// ---------------------------------------
// その他
// ---------------------------------------
/* ボタン活性／非活性 */
function avail(){
    $('button').removeAttr('disabled');
    $('a').removeAttr('disabled');
    $("#loading").empty();
    loading = '0';
}

/* ペイン開閉 */
$("#btnClosePc").click(function(){
    $("#pcBox").slideToggle(500);
});
$("#btnCloseNonPc").click(function(){
    $("#nonPcBox").slideToggle(500);
});
$("#btnCloseAdd").click(function(){
    $("#addBox").slideToggle(500);
});

/* ソート */
$(function(){
    $('table').tablesorter({
        widgets        : ['zebra', 'columns'],
        usNumberFormat : false,
        sortReset      : true,
        sortRestart    : true
    });
});




